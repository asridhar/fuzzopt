from optimizer import OPTIMAL, CplexSolver, GurobiSolver, CBCSolver
import glob
import os
import sys
from mps_util import eprint

MIPEmphasisBalanced = 0
MIPEmphasisFeasibility = 1
MIPEmphasisOptimality = 2
MIPEmphasisBestBound = 3
CPX_MIPEMPHASIS_HIDDENFEAS = 4
MIPEmphasisHeuristic = 5

def replay(path):
    cpx = CplexSolver(timeout=False, emphasis=MIPEmphasisBestBound)
    grb = GurobiSolver(timeout=False)
    diff_mps = glob.glob(os.path.join(path, "*.mps"))
    if len(diff_mps) == 0:
        diff_mps = [path]
    eprint(diff_mps)
    for mps in diff_mps:
        eprint(mps)
        
        cpx_sol = cpx(mps)
        eprint("Cplex:  Status = ", cpx_sol["status"], end="")
        if cpx_sol["status"] == OPTIMAL:
            eprint(", Obj Val = ", cpx_sol["obj_val"])
        else:
            eprint("")
        
        grb_sol = grb(mps)
        eprint("Gurobi: Status = ", grb_sol["status"], end="\n\n" if grb_sol["status"] != OPTIMAL else "")
        if grb_sol["status"] == OPTIMAL:
            eprint(", Obj Val = ", grb_sol["obj_val"], "\n")

        cbc_solution(mps)


def cbc_solution(mps):
    cbc = CBCSolver()
    cbc_sol = cbc(mps)
    eprint("CBC: Status = ", cbc_sol["status"], end="\n\n" if cbc_sol["status"] != OPTIMAL else "")
    if cbc_sol["status"] == OPTIMAL:
        eprint(", Obj Val = ", cbc_sol["obj_val"], "\n")

if __name__ == "__main__":
    assert len(sys.argv) >= 2, "Number of arguments must be larger than 2"
    replay(sys.argv[1])
