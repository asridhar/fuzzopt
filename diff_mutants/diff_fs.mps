NAME enlight_hard
ROWS
  N OBJ
  E inner_area_1 
  E inner_area_2 
  E inner_area_6 
  E inner_area_7 
  E inner_area_8 
  E inner_area_9 
  E inner_area_10 
  E inner_area_11 
  E inner_area_14 
  E inner_area_15 
  E inner_area_16 
  E inner_area_17 
  E inner_area_21 
  E inner_area_24 
  E inner_area_25 
  E inner_area_27 
  E inner_area_28 
  E inner_area_29 
  E inner_area_31 
  E inner_area_32 
  E inner_area_33 
  E inner_area_34 
  E inner_area_35 
  E inner_area_36 
  E inner_area_37 
  E inner_area_38 
  E inner_area_39 
  E inner_area_41 
  E inner_area_45 
  E inner_area_46 
  E inner_area_47 
  E inner_area_48 
  E inner_area_49 
  E inner_area_50 
  E inner_area_51 
  E inner_area_52 
  E inner_area_54 
  E inner_area_55 
  E inner_area_56 
  E inner_area_59 
  E inner_area_60 
  E inner_area_61 
  E upper_border_1 
  E upper_border_4 
  E upper_border_5 
  E upper_border_6 
  E upper_border_7 
  E upper_border_8 
  E lower_border_1 
  E lower_border_2 
  E lower_border_4 
  E lower_border_5 
  E lower_border_7 
  E left_border_1 
  E left_border_2 
  E left_border_5 
  E right_border_2 
  E right_border_3 
  E right_border_5 
  E right_border_6 
  E left_upper_co@60 
  E right_upper_c@62 
  E right_lower_c@63 
COLUMNS
     MARK0000  'MARKER'                 'INTORG'
     x#1#1		OBJ		1.0
     x#1#1		upper_border_1		1.0
     x#1#1		left_border_1		-1.0
     x#1#1		left_upper_co@60		-1.0
     x#1#2		OBJ		1.0
     x#1#2		inner_area_1		-1.0
     x#1#2		upper_border_1		1.0
     x#1#2		left_upper_co@60		1.0
     x#1#3		OBJ		1.0
     x#1#3		inner_area_2		1.0
     x#1#3		upper_border_1		1.0
     x#1#4		OBJ		1.0
     x#1#4		upper_border_4		1.0
     x#1#5		OBJ		1.0
     x#1#5		upper_border_4		-1.0
     x#1#5		upper_border_5		1.0
     x#1#6		OBJ		1.0
     x#1#6		upper_border_4		-1.0
     x#1#6		upper_border_5		1.0
     x#1#6		upper_border_6		1.0
     x#1#7		OBJ		1.0
     x#1#7		inner_area_6		1.0
     x#1#7		upper_border_5		-1.0
     x#1#7		upper_border_6		-1.0
     x#1#7		upper_border_7		-1.0
     x#1#8		OBJ		1.0
     x#1#8		inner_area_7		1.0
     x#1#8		upper_border_6		-1.0
     x#1#8		upper_border_7		-1.0
     x#1#8		upper_border_8		-1.0
     x#1#9		OBJ		1.0
     x#1#9		inner_area_8		1.0
     x#1#9		upper_border_7		1.0
     x#1#9		upper_border_8		1.0
     x#1#9		right_upper_c@62		1.0
     x#1#10		OBJ		1.0
     x#1#10		upper_border_8		-1.0
     x#1#10		right_upper_c@62		-1.0
     x#2#1		OBJ		1.0
     x#2#1		inner_area_1		1.0
     x#2#1		left_border_1		1.0
     x#2#1		left_border_2		1.0
     x#2#1		left_upper_co@60		1.0
     x#2#2		OBJ		1.0
     x#2#2		inner_area_1		-1.0
     x#2#2		inner_area_2		-1.0
     x#2#2		inner_area_9		1.0
     x#2#2		upper_border_1		-1.0
     x#2#2		left_border_1		-1.0
     x#2#3		OBJ		1.0
     x#2#3		inner_area_1		1.0
     x#2#3		inner_area_2		1.0
     x#2#3		inner_area_10		1.0
     x#2#4		OBJ		1.0
     x#2#4		inner_area_2		-1.0
     x#2#4		inner_area_11		-1.0
     x#2#5		OBJ		1.0
     x#2#5		upper_border_4		1.0
     x#2#6		OBJ		1.0
     x#2#6		inner_area_6		1.0
     x#2#6		upper_border_5		-1.0
     x#2#7		OBJ		1.0
     x#2#7		inner_area_6		1.0
     x#2#7		inner_area_7		1.0
     x#2#7		inner_area_14		1.0
     x#2#7		upper_border_6		1.0
     x#2#8		OBJ		1.0
     x#2#8		inner_area_6		1.0
     x#2#8		inner_area_7		-1.0
     x#2#8		inner_area_8		1.0
     x#2#8		inner_area_15		-1.0
     x#2#8		upper_border_7		1.0
     x#2#9		OBJ		1.0
     x#2#9		inner_area_7		-1.0
     x#2#9		inner_area_8		1.0
     x#2#9		inner_area_16		1.0
     x#2#9		upper_border_8		1.0
     x#2#10		OBJ		1.0
     x#2#10		inner_area_8		-1.0
     x#2#10		right_border_2		-1.0
     x#2#10		right_upper_c@62		1.0
     x#3#1		OBJ		1.0
     x#3#1		inner_area_9		1.0
     x#3#1		left_border_1		-1.0
     x#3#1		left_border_2		-1.0
     x#3#2		OBJ		1.0
     x#3#2		inner_area_1		1.0
     x#3#2		inner_area_9		1.0
     x#3#2		inner_area_10		1.0
     x#3#2		inner_area_17		1.0
     x#3#2		left_border_2		-1.0
     x#3#3		OBJ		1.0
     x#3#3		inner_area_2		1.0
     x#3#3		inner_area_9		-1.0
     x#3#3		inner_area_10		-1.0
     x#3#3		inner_area_11		-1.0
     x#3#4		OBJ		1.0
     x#3#4		inner_area_10		-1.0
     x#3#4		inner_area_11		1.0
     x#3#5		OBJ		1.0
     x#3#5		inner_area_11		-1.0
     x#3#6		OBJ		1.0
     x#3#6		inner_area_14		1.0
     x#3#6		inner_area_21		1.0
     x#3#7		OBJ		1.0
     x#3#7		inner_area_6		1.0
     x#3#7		inner_area_14		1.0
     x#3#7		inner_area_15		-1.0
     x#3#8		OBJ		1.0
     x#3#8		inner_area_7		-1.0
     x#3#8		inner_area_14		1.0
     x#3#8		inner_area_15		1.0
     x#3#8		inner_area_16		-1.0
     x#3#9		OBJ		1.0
     x#3#9		inner_area_8		-1.0
     x#3#9		inner_area_15		1.0
     x#3#9		inner_area_16		-1.0
     x#3#9		inner_area_24		1.0
     x#3#9		right_border_2		1.0
     x#3#10		OBJ		1.0
     x#3#10		inner_area_16		-1.0
     x#3#10		right_border_2		-1.0
     x#3#10		right_border_3		1.0
     x#4#1		OBJ		1.0
     x#4#1		inner_area_17		-1.0
     x#4#1		left_border_2		1.0
     x#4#2		OBJ		1.0
     x#4#2		inner_area_9		-1.0
     x#4#2		inner_area_17		1.0
     x#4#2		inner_area_25		1.0
     x#4#3		OBJ		1.0
     x#4#3		inner_area_10		-1.0
     x#4#3		inner_area_17		-1.0
     x#4#4		OBJ		1.0
     x#4#4		inner_area_11		1.0
     x#4#4		inner_area_27		1.0
     x#4#5		OBJ		1.0
     x#4#5		inner_area_21		-1.0
     x#4#5		inner_area_28		1.0
     x#4#6		OBJ		1.0
     x#4#6		inner_area_21		1.0
     x#4#6		inner_area_29		-1.0
     x#4#7		OBJ		1.0
     x#4#7		inner_area_14		-1.0
     x#4#7		inner_area_21		1.0
     x#4#8		OBJ		1.0
     x#4#8		inner_area_15		-1.0
     x#4#8		inner_area_24		1.0
     x#4#8		inner_area_31		1.0
     x#4#9		OBJ		1.0
     x#4#9		inner_area_16		-1.0
     x#4#9		inner_area_24		1.0
     x#4#9		inner_area_32		-1.0
     x#4#9		right_border_3		1.0
     x#4#10		OBJ		1.0
     x#4#10		inner_area_24		-1.0
     x#4#10		right_border_2		1.0
     x#4#10		right_border_3		1.0
     x#5#1		OBJ		1.0
     x#5#1		inner_area_25		-1.0
     x#5#1		left_border_5		-1.0
     x#5#2		OBJ		1.0
     x#5#2		inner_area_17		1.0
     x#5#2		inner_area_25		1.0
     x#5#2		inner_area_33		1.0
     x#5#3		OBJ		1.0
     x#5#3		inner_area_25		1.0
     x#5#3		inner_area_27		-1.0
     x#5#3		inner_area_34		-1.0
     x#5#4		OBJ		1.0
     x#5#4		inner_area_27		1.0
     x#5#4		inner_area_28		1.0
     x#5#4		inner_area_35		1.0
     x#5#5		OBJ		1.0
     x#5#5		inner_area_27		1.0
     x#5#5		inner_area_28		1.0
     x#5#5		inner_area_29		1.0
     x#5#5		inner_area_36		1.0
     x#5#6		OBJ		1.0
     x#5#6		inner_area_21		1.0
     x#5#6		inner_area_28		1.0
     x#5#6		inner_area_29		-1.0
     x#5#6		inner_area_37		-1.0
     x#5#7		OBJ		1.0
     x#5#7		inner_area_29		1.0
     x#5#7		inner_area_31		-1.0
     x#5#7		inner_area_38		-1.0
     x#5#8		OBJ		1.0
     x#5#8		inner_area_31		-1.0
     x#5#8		inner_area_32		-1.0
     x#5#8		inner_area_39		1.0
     x#5#9		OBJ		1.0
     x#5#9		inner_area_24		1.0
     x#5#9		inner_area_31		1.0
     x#5#9		inner_area_32		1.0
     x#5#10		OBJ		1.0
     x#5#10		inner_area_32		-1.0
     x#5#10		right_border_3		1.0
     x#5#10		right_border_5		1.0
     x#6#1		OBJ		1.0
     x#6#1		inner_area_33		1.0
     x#6#1		left_border_5		1.0
     x#6#2		OBJ		1.0
     x#6#2		inner_area_25		1.0
     x#6#2		inner_area_33		-1.0
     x#6#2		inner_area_34		-1.0
     x#6#2		inner_area_41		-1.0
     x#6#2		left_border_5		1.0
     x#6#3		OBJ		1.0
     x#6#3		inner_area_33		1.0
     x#6#3		inner_area_34		1.0
     x#6#3		inner_area_35		1.0
     x#6#4		OBJ		1.0
     x#6#4		inner_area_27		1.0
     x#6#4		inner_area_34		-1.0
     x#6#4		inner_area_35		-1.0
     x#6#4		inner_area_36		1.0
     x#6#5		OBJ		1.0
     x#6#5		inner_area_28		-1.0
     x#6#5		inner_area_35		1.0
     x#6#5		inner_area_36		-1.0
     x#6#5		inner_area_37		1.0
     x#6#6		OBJ		1.0
     x#6#6		inner_area_29		-1.0
     x#6#6		inner_area_36		1.0
     x#6#6		inner_area_37		1.0
     x#6#6		inner_area_38		-1.0
     x#6#6		inner_area_45		1.0
     x#6#7		OBJ		1.0
     x#6#7		inner_area_37		1.0
     x#6#7		inner_area_38		1.0
     x#6#7		inner_area_39		-1.0
     x#6#7		inner_area_46		-1.0
     x#6#8		OBJ		1.0
     x#6#8		inner_area_31		1.0
     x#6#8		inner_area_38		1.0
     x#6#8		inner_area_39		-1.0
     x#6#8		inner_area_47		1.0
     x#6#9		OBJ		1.0
     x#6#9		inner_area_32		1.0
     x#6#9		inner_area_39		-1.0
     x#6#9		inner_area_48		1.0
     x#6#9		right_border_5		1.0
     x#6#10		OBJ		1.0
     x#6#10		right_border_5		1.0
     x#6#10		right_border_6		-1.0
     x#7#1		OBJ		1.0
     x#7#1		inner_area_41		1.0
     x#7#1		left_border_5		1.0
     x#7#2		OBJ		1.0
     x#7#2		inner_area_33		-1.0
     x#7#2		inner_area_41		-1.0
     x#7#2		inner_area_49		-1.0
     x#7#3		OBJ		1.0
     x#7#3		inner_area_34		-1.0
     x#7#3		inner_area_41		1.0
     x#7#3		inner_area_50		1.0
     x#7#4		OBJ		1.0
     x#7#4		inner_area_35		1.0
     x#7#4		inner_area_51		1.0
     x#7#5		OBJ		1.0
     x#7#5		inner_area_36		1.0
     x#7#5		inner_area_45		-1.0
     x#7#5		inner_area_52		1.0
     x#7#6		OBJ		1.0
     x#7#6		inner_area_37		-1.0
     x#7#6		inner_area_45		-1.0
     x#7#6		inner_area_46		1.0
     x#7#7		OBJ		1.0
     x#7#7		inner_area_38		-1.0
     x#7#7		inner_area_45		1.0
     x#7#7		inner_area_46		1.0
     x#7#7		inner_area_47		1.0
     x#7#7		inner_area_54		-1.0
     x#7#8		OBJ		1.0
     x#7#8		inner_area_39		-1.0
     x#7#8		inner_area_46		-1.0
     x#7#8		inner_area_47		-1.0
     x#7#8		inner_area_48		-1.0
     x#7#8		inner_area_55		-1.0
     x#7#9		OBJ		1.0
     x#7#9		inner_area_47		-1.0
     x#7#9		inner_area_48		-1.0
     x#7#9		inner_area_56		1.0
     x#7#9		right_border_6		1.0
     x#7#10		OBJ		1.0
     x#7#10		inner_area_48		-1.0
     x#7#10		right_border_5		-1.0
     x#7#10		right_border_6		-1.0
     x#8#1		OBJ		1.0
     x#8#1		inner_area_49		-1.0
     x#8#2		OBJ		1.0
     x#8#2		inner_area_41		-1.0
     x#8#2		inner_area_49		-1.0
     x#8#2		inner_area_50		-1.0
     x#8#3		OBJ		1.0
     x#8#3		inner_area_49		1.0
     x#8#3		inner_area_50		-1.0
     x#8#3		inner_area_51		1.0
     x#8#4		OBJ		1.0
     x#8#4		inner_area_50		1.0
     x#8#4		inner_area_51		1.0
     x#8#4		inner_area_52		1.0
     x#8#4		inner_area_59		1.0
     x#8#5		OBJ		1.0
     x#8#5		inner_area_51		-1.0
     x#8#5		inner_area_52		-1.0
     x#8#5		inner_area_60		1.0
     x#8#6		OBJ		1.0
     x#8#6		inner_area_45		1.0
     x#8#6		inner_area_52		1.0
     x#8#6		inner_area_54		1.0
     x#8#6		inner_area_61		-1.0
     x#8#7		OBJ		1.0
     x#8#7		inner_area_46		-1.0
     x#8#7		inner_area_54		1.0
     x#8#7		inner_area_55		-1.0
     x#8#8		OBJ		1.0
     x#8#8		inner_area_47		-1.0
     x#8#8		inner_area_54		1.0
     x#8#8		inner_area_55		-1.0
     x#8#8		inner_area_56		1.0
     x#8#9		OBJ		1.0
     x#8#9		inner_area_48		-1.0
     x#8#9		inner_area_55		-1.0
     x#8#9		inner_area_56		1.0
     x#8#10		OBJ		1.0
     x#8#10		inner_area_56		1.0
     x#8#10		right_border_6		-1.0
     x#9#2		OBJ		1.0
     x#9#2		inner_area_49		1.0
     x#9#2		lower_border_1		-1.0
     x#9#3		OBJ		1.0
     x#9#3		inner_area_50		-1.0
     x#9#3		inner_area_59		1.0
     x#9#3		lower_border_2		-1.0
     x#9#4		OBJ		1.0
     x#9#4		inner_area_51		1.0
     x#9#4		inner_area_59		1.0
     x#9#4		inner_area_60		-1.0
     x#9#5		OBJ		1.0
     x#9#5		inner_area_52		1.0
     x#9#5		inner_area_59		-1.0
     x#9#5		inner_area_60		-1.0
     x#9#5		inner_area_61		1.0
     x#9#5		lower_border_4		-1.0
     x#9#6		OBJ		1.0
     x#9#6		inner_area_60		1.0
     x#9#6		inner_area_61		-1.0
     x#9#6		lower_border_5		-1.0
     x#9#7		OBJ		1.0
     x#9#7		inner_area_54		1.0
     x#9#7		inner_area_61		-1.0
     x#9#8		OBJ		1.0
     x#9#8		inner_area_55		1.0
     x#9#8		lower_border_7		1.0
     x#9#9		OBJ		1.0
     x#9#9		inner_area_56		-1.0
     x#9#10		OBJ		1.0
     x#9#10		right_lower_c@63		-1.0
     x#10#1		OBJ		1.0
     x#10#1		lower_border_1		1.0
     x#10#2		OBJ		1.0
     x#10#2		lower_border_1		-1.0
     x#10#2		lower_border_2		1.0
     x#10#3		OBJ		1.0
     x#10#3		lower_border_1		1.0
     x#10#3		lower_border_2		-1.0
     x#10#4		OBJ		1.0
     x#10#4		inner_area_59		1.0
     x#10#4		lower_border_2		1.0
     x#10#4		lower_border_4		-1.0
     x#10#5		OBJ		1.0
     x#10#5		inner_area_60		-1.0
     x#10#5		lower_border_4		1.0
     x#10#5		lower_border_5		1.0
     x#10#6		OBJ		1.0
     x#10#6		inner_area_61		-1.0
     x#10#6		lower_border_4		-1.0
     x#10#6		lower_border_5		-1.0
     x#10#7		OBJ		1.0
     x#10#7		lower_border_5		1.0
     x#10#7		lower_border_7		-1.0
     x#10#8		OBJ		1.0
     x#10#8		lower_border_7		-1.0
     x#10#9		OBJ		1.0
     x#10#9		lower_border_7		1.0
     x#10#9		right_lower_c@63		-1.0
     x#10#10		OBJ		1.0
     x#10#10		right_lower_c@63		1.0
     y#2#2		inner_area_1		-2.0
     y#2#3		inner_area_2		-2.0
     y#2#7		inner_area_6		-2.0
     y#2#8		inner_area_7		2.0
     y#2#9		inner_area_8		2.0
     y#3#2		inner_area_9		-2.0
     y#3#3		inner_area_10		2.0
     y#3#4		inner_area_11		2.0
     y#3#7		inner_area_14		2.0
     y#3#8		inner_area_15		2.0
     y#3#9		inner_area_16		-2.0
     y#4#2		inner_area_17		-2.0
     y#4#6		inner_area_21		2.0
     y#4#9		inner_area_24		-2.0
     y#5#2		inner_area_25		2.0
     y#5#4		inner_area_27		2.0
     y#5#5		inner_area_28		-2.0
     y#5#6		inner_area_29		-2.0
     y#5#8		inner_area_31		2.0
     y#5#9		inner_area_32		-2.0
     y#6#2		inner_area_33		-2.0
     y#6#3		inner_area_34		-2.0
     y#6#4		inner_area_35		2.0
     y#6#5		inner_area_36		2.0
     y#6#6		inner_area_37		-2.0
     y#6#7		inner_area_38		-2.0
     y#6#8		inner_area_39		2.0
     y#7#2		inner_area_41		-2.0
     y#7#6		inner_area_45		2.0
     y#7#7		inner_area_46		2.0
     y#7#8		inner_area_47		2.0
     y#7#9		inner_area_48		-2.0
     y#8#2		inner_area_49		-2.0
     y#8#3		inner_area_50		-2.0
     y#8#4		inner_area_51		-2.0
     y#8#5		inner_area_52		-2.0
     y#8#7		inner_area_54		2.0
     y#8#8		inner_area_55		-2.0
     y#8#9		inner_area_56		2.0
     y#9#4		inner_area_59		-2.0
     y#9#5		inner_area_60		-2.0
     y#9#6		inner_area_61		2.0
     y#1#2		upper_border_1		2.0
     y#1#5		upper_border_4		-2.0
     y#1#6		upper_border_5		-2.0
     y#1#7		upper_border_6		-2.0
     y#1#8		upper_border_7		-2.0
     y#1#9		upper_border_8		2.0
     y#10#2		lower_border_1		2.0
     y#10#3		lower_border_2		2.0
     y#10#5		lower_border_4		-2.0
     y#10#6		lower_border_5		-2.0
     y#10#8		lower_border_7		2.0
     y#2#1		left_border_1		2.0
     y#3#1		left_border_2		-2.0
     y#6#1		left_border_5		2.0
     y#3#10		right_border_2		-2.0
     y#4#10		right_border_3		2.0
     y#6#10		right_border_5		-2.0
     y#7#10		right_border_6		2.0
     y#1#1		left_upper_co@60		2.0
     y#1#10		right_upper_c@62		-2.0
     y#10#10		right_lower_c@63		-2.0
     MARK0000  'MARKER'                 'INTEND'
RHS
    RHS1		inner_area_1		0.0
    RHS1		inner_area_2		0.0
    RHS1		inner_area_6		-1.0
    RHS1		inner_area_7		0.0
    RHS1		inner_area_8		0.0
    RHS1		inner_area_9		-1.0
    RHS1		inner_area_10		0.0
    RHS1		inner_area_11		0.0
    RHS1		inner_area_14		0.0
    RHS1		inner_area_15		0.0
    RHS1		inner_area_16		0.0
    RHS1		inner_area_17		0.0
    RHS1		inner_area_21		0.0
    RHS1		inner_area_24		0.0
    RHS1		inner_area_25		0.0
    RHS1		inner_area_27		0.0
    RHS1		inner_area_28		0.0
    RHS1		inner_area_29		-1.0
    RHS1		inner_area_31		0.0
    RHS1		inner_area_32		0.0
    RHS1		inner_area_33		0.0
    RHS1		inner_area_34		-1.0
    RHS1		inner_area_35		0.0
    RHS1		inner_area_36		0.0
    RHS1		inner_area_37		0.0
    RHS1		inner_area_38		0.0
    RHS1		inner_area_39		-1.0
    RHS1		inner_area_41		0.0
    RHS1		inner_area_45		0.0
    RHS1		inner_area_46		-1.0
    RHS1		inner_area_47		0.0
    RHS1		inner_area_48		-1.0
    RHS1		inner_area_49		0.0
    RHS1		inner_area_50		0.0
    RHS1		inner_area_51		0.0
    RHS1		inner_area_52		-1.0
    RHS1		inner_area_54		0.0
    RHS1		inner_area_55		-1.0
    RHS1		inner_area_56		0.0
    RHS1		inner_area_59		0.0
    RHS1		inner_area_60		0.0
    RHS1		inner_area_61		0.0
    RHS1		upper_border_1		0.0
    RHS1		upper_border_4		0.0
    RHS1		upper_border_5		0.0
    RHS1		upper_border_6		0.0
    RHS1		upper_border_7		-1.0
    RHS1		upper_border_8		0.0
    RHS1		lower_border_1		0.0
    RHS1		lower_border_2		-1.0
    RHS1		lower_border_4		-1.0
    RHS1		lower_border_5		0.0
    RHS1		lower_border_7		0.0
    RHS1		left_border_1		0.0
    RHS1		left_border_2		0.0
    RHS1		left_border_5		0.0
    RHS1		right_border_2		-1.0
    RHS1		right_border_3		0.0
    RHS1		right_border_5		0.0
    RHS1		right_border_6		0.0
    RHS1		left_upper_co@60		-1.0
    RHS1		right_upper_c@62		0.0
    RHS1		right_lower_c@63		0.0
BOUNDS
 UI BND1		x#1#2		1
 UI BND1		x#1#3		1
 UI BND1		x#1#4		1
 UI BND1		x#1#5		1
 UI BND1		x#1#6		1
 UI BND1		x#1#7		1
 UI BND1		x#1#8		1
 UI BND1		x#1#9		1
 UI BND1		x#1#10		1
 UI BND1		x#2#1		1
 UI BND1		x#2#2		1
 UI BND1		x#2#3		1
 UI BND1		x#2#4		1
 UI BND1		x#2#5		1
 UI BND1		x#2#6		1
 UI BND1		x#2#7		1
 UI BND1		x#2#8		1
 UI BND1		x#2#9		1
 UI BND1		x#3#2		1
 UI BND1		x#3#3		1
 UI BND1		x#3#4		1
 UI BND1		x#3#6		1
 UI BND1		x#3#7		1
 UI BND1		x#3#8		1
 UI BND1		x#3#9		1
 UI BND1		x#4#1		1
 UI BND1		x#4#2		1
 UI BND1		x#4#3		1
 UI BND1		x#4#4		1
 UI BND1		x#4#5		1
 UI BND1		x#4#6		1
 UI BND1		x#4#7		1
 UI BND1		x#4#8		1
 UI BND1		x#4#9		1
 UI BND1		x#4#10		1
 UI BND1		x#5#1		1
 UI BND1		x#5#2		1
 UI BND1		x#5#4		1
 UI BND1		x#5#5		1
 UI BND1		x#5#6		1
 UI BND1		x#5#9		1
 UI BND1		x#5#10		1
 UI BND1		x#6#1		1
 UI BND1		x#6#3		1
 UI BND1		x#6#4		1
 UI BND1		x#6#5		1
 UI BND1		x#6#6		1
 UI BND1		x#6#7		1
 UI BND1		x#6#8		1
 UI BND1		x#6#9		1
 UI BND1		x#6#10		1
 UI BND1		x#7#1		1
 UI BND1		x#7#2		1
 UI BND1		x#7#3		1
 UI BND1		x#7#4		1
 UI BND1		x#7#5		1
 UI BND1		x#7#7		1
 UI BND1		x#7#8		1
 UI BND1		x#7#9		1
 UI BND1		x#7#10		1
 UI BND1		x#8#2		1
 UI BND1		x#8#3		1
 UI BND1		x#8#4		1
 UI BND1		x#8#6		1
 UI BND1		x#8#8		1
 UI BND1		x#8#10		1
 UI BND1		x#9#2		1
 UI BND1		x#9#4		1
 UI BND1		x#9#7		1
 UI BND1		x#9#8		1
 UI BND1		x#10#1		1
 UI BND1		x#10#2		1
 UI BND1		x#10#3		1
 UI BND1		x#10#4		1
 UI BND1		x#10#5		1
 UI BND1		x#10#6		1
 UI BND1		x#10#7		1
 UI BND1		x#10#9		1
 UI BND1		x#10#10		1
ENDATA