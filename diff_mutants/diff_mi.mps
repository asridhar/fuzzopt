NAME markshare_4_0
ROWS
  N MINIMIZE
  E C1_ 
  E C2_ 
  E C3_ 
  E C4_ 
COLUMNS
     s1		MINIMIZE		1.0
     s1		C1_		1.0
     MARK0000  'MARKER'                 'INTORG'
     s2		MINIMIZE		1.0
     s2		C2_		1.0
     s3		MINIMIZE		1.0
     s3		C3_		1.0
     MARK0000  'MARKER'                 'INTEND'
     s4		MINIMIZE		1.0
     s4		C4_		1.0
     MARK0001  'MARKER'                 'INTORG'
     x1		C1_		17.0
     x1		C2_		93.0
     x1		C3_		46.0
     x1		C4_		2.0
     x2		C1_		75.0
     x2		C2_		44.0
     x2		C3_		63.0
     x2		C4_		77.0
     x3		C1_		9.0
     x3		C2_		79.0
     x3		C3_		13.0
     x3		C4_		73.0
     x4		C1_		87.0
     x4		C2_		12.0
     x4		C3_		97.0
     x4		C4_		59.0
     x5		C1_		58.0
     x5		C2_		8.0
     x5		C3_		14.0
     x5		C4_		43.0
     x6		C1_		79.0
     x6		C2_		95.0
     x6		C3_		45.0
     x6		C4_		64.0
     x7		C1_		69.0
     x7		C2_		2.0
     x7		C3_		32.0
     x7		C4_		75.0
     x8		C1_		37.0
     x8		C2_		15.0
     x8		C3_		96.0
     x8		C4_		6.0
     x9		C1_		88.0
     x9		C2_		38.0
     x9		C3_		36.0
     x9		C4_		5.0
     x10		C1_		75.0
     x10		C2_		15.0
     x10		C3_		40.0
     x10		C4_		78.0
     x11		C1_		45.0
     x11		C2_		53.0
     x11		C3_		10.0
     x11		C4_		71.0
     x12		C1_		35.0
     x12		C2_		88.0
     x12		C3_		96.0
     x12		C4_		12.0
     x13		C1_		73.0
     x13		C2_		43.0
     x13		C3_		99.0
     x13		C4_		30.0
     x14		C1_		26.0
     x14		C2_		26.0
     x14		C3_		58.0
     x14		C4_		7.0
     x15		C1_		39.0
     x15		C2_		31.0
     x15		C3_		87.0
     x15		C4_		69.0
     x16		C1_		78.0
     x16		C2_		77.0
     x16		C3_		15.0
     x16		C4_		36.0
     x17		C1_		85.0
     x17		C2_		10.0
     x17		C3_		91.0
     x17		C4_		73.0
     x18		C1_		58.0
     x18		C2_		77.0
     x18		C3_		65.0
     x18		C4_		19.0
     x19		C1_		72.0
     x19		C2_		71.0
     x19		C3_		6.0
     x19		C4_		15.0
     x20		C1_		8.0
     x20		C2_		22.0
     x20		C3_		96.0
     x20		C4_		16.0
     x21		C1_		46.0
     x21		C2_		76.0
     x21		C3_		97.0
     x21		C4_		84.0
     x22		C1_		11.0
     x22		C2_		41.0
     x22		C3_		79.0
     x22		C4_		55.0
     x23		C1_		55.0
     x23		C2_		65.0
     x23		C3_		81.0
     x23		C4_		32.0
     x24		C1_		39.0
     x24		C2_		93.0
     x24		C3_		57.0
     x24		C4_		53.0
     x25		C1_		57.0
     x25		C2_		50.0
     x25		C3_		28.0
     x25		C4_		43.0
     x26		C1_		96.0
     x26		C2_		69.0
     x26		C3_		97.0
     x26		C4_		21.0
     x27		C1_		87.0
     x27		C2_		44.0
     x27		C3_		58.0
     x27		C4_		73.0
     x28		C1_		16.0
     x28		C2_		61.0
     x28		C3_		44.0
     x29		C1_		27.0
     x29		C2_		58.0
     x29		C3_		37.0
     x29		C4_		59.0
     x30		C1_		26.0
     x30		C2_		63.0
     x30		C3_		93.0
     x30		C4_		48.0
     MARK0001  'MARKER'                 'INTEND'
RHS
    RHS		C1_		786.0
    RHS		C2_		759.0
    RHS		C3_		888.0
    RHS		C4_		649.0
BOUNDS
 UI bnd		x1		1
 UI bnd		x2		1
 UI bnd		x3		1
 UI bnd		x4		1
 UI bnd		x5		1
 UI bnd		x6		1
 UI bnd		x7		1
 UI bnd		x8		1
 UI bnd		x9		1
 UI bnd		x10		1
 UI bnd		x11		1
 UI bnd		x12		1
 UI bnd		x13		1
 UI bnd		x14		1
 UI bnd		x15		1
 UI bnd		x16		1
 UI bnd		x17		1
 UI bnd		x18		1
 UI bnd		x19		1
 UI bnd		x20		1
 UI bnd		x21		1
 UI bnd		x22		1
 UI bnd		x23		1
 UI bnd		x24		1
 UI bnd		x25		1
 UI bnd		x26		1
 UI bnd		x27		1
 UI bnd		x28		1
 UI bnd		x29		1
 UI bnd		x30		1
ENDATA