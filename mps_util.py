import os
import sys
from pysmps.pysmps.smps_loader import load_mps
from generator import MPSProgram

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# A simple wrapper to suppress talkative APIs from printing
# Inspired from https://stackoverflow.com/a/45669280
class VerbosePrint:
    def __init__(self, verbose, disable_stderr = False) -> None:
        self.verbose = verbose
        self.disable_stderr = disable_stderr
    
    def __enter__(self):
        if not self.verbose:
            self._original_stdout = sys.stdout
            sys.stdout = open(os.devnull, 'w')
            if self.disable_stderr:
                self._original_stderr = sys.stderr
                sys.stderr = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.verbose:
            sys.stdout.close()
            sys.stdout = self._original_stdout
            if self.disable_stderr:
                sys.stderr = self._original_stderr

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def load_mps_dict(path):
    name, objsense, objective_name, row_names, col_names, col_types, types, c, A, rhs_names, rhs, bnd_names, bnd = load_mps(path)
    ret = dict()
    ret["name"] = name
    ret["objsense"] = objsense
    ret["obj_name"] = objective_name
    ret["row_names"] = row_names
    ret["col_names"] = col_names
    ret["col_types"] = col_types
    ret["types"] = types
    ret["obj_coeff"] = c
    ret["con_mat"] = A
    ret["rhs_names"] = rhs_names
    ret["rhs"] = rhs
    ret["bnd_names"] = bnd_names
    ret["bnd"] = bnd
    return ret

def print_mps_dict(mps: dict):
    for k in mps:
        print(f"{bcolors.OKBLUE}{k}\n{bcolors.ENDC}", mps[k])

def print_load_mps_ret(path):
    name, objsense, objective_name, row_names, col_names, col_types, types, c, A, rhs_names, rhs, bnd_names, bnd = load_mps(path)
    print(f"{bcolors.OKBLUE}Program Name: {bcolors.ENDC}", name)
    print(f"{bcolors.OKBLUE}Objective Sense: {bcolors.ENDC}", objsense)
    print(f"{bcolors.OKBLUE}Objective Name: {bcolors.ENDC}", objective_name)
    print(f"{bcolors.OKBLUE}Row Names: {bcolors.ENDC}", row_names)
    print(f"{bcolors.OKBLUE}Column Names: {bcolors.ENDC}", col_names)
    print(f"{bcolors.OKBLUE}Column Types: {bcolors.ENDC}", col_types)
    print(f"{bcolors.OKBLUE}Types: {bcolors.ENDC}", types)
    print(f"{bcolors.OKBLUE}Objective Coeff: {bcolors.ENDC}", c)
    print(f"{bcolors.OKBLUE}Constraint Matrix\n{bcolors.ENDC}", A)
    print(f"{bcolors.OKBLUE}RHS Names: {bcolors.ENDC}", rhs_names)
    print(f"{bcolors.OKBLUE}RHS\n{bcolors.ENDC}", rhs)
    print(f"{bcolors.OKBLUE}Bound Names: {bcolors.ENDC}", bnd_names)
    print(f"{bcolors.OKBLUE}Bound\n{bcolors.ENDC}", bnd)

def main():
    assert len(sys.argv) >= 2, "Number of arguments must be larger than 2"
    print_load_mps_ret(sys.argv[1])
    print(MPSProgram(**load_mps_dict(sys.argv[1])))

if __name__ == "__main__":
    main()