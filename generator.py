import random
import numpy as np
import sys
import subprocess as sp

class MPSProgram:
    """ 
    Class to represent MPS programs. 
    """ 
    # def __init__(self, 
    #              name, 
    #              objsense, 
    #              objective_name, 
    #              row_names, 
    #              col_names, 
    #              col_types, 
    #              types, 
    #              c, 
    #              A, 
    #              rhs_names,
    #              rhs, 
    #              bnd_names, 
    #              bnd
    #              ):     
    def __init__(self, 
                 **kwargs
                 ): 
        self.name = kwargs["name"]
        self.objsense = kwargs["objsense"]
        self.objective_name = kwargs["obj_name"]
        self.row_names = kwargs["row_names"]
        self.col_names = kwargs["col_names"]
        self.col_types = kwargs["col_types"]
        self.types = kwargs["types"] 
        self.c = kwargs["obj_coeff"] 
        self.A = kwargs["con_mat"]
        self.rhs_names = kwargs["rhs_names"] 
        self.rhs = kwargs["rhs"]  
        self.bnd_names = kwargs["bnd_names"] 
        self.bnd = kwargs["bnd"]

    def shift(self, prefix): 
        self.col_names = [prefix+"_"+c for c in self.col_names] 
        self.row_names = [prefix+"_"+c for c in self.row_names] 
        self.rhs_names = [prefix+"_"+r for r in self.rhs_names] 
        self.rhs = {prefix+"_"+k: v for k, v in self.rhs.items()}

    def to_string(self):
        out = "NAME "+ self.name + "\n"
        if self.objsense != "":
            out += "OBJSENSE \n  " + self.objsense + "\n"
        out+="ROWS\n"
        out+="  N {}\n".format(self.objective_name)
        for i,row in enumerate(self.row_names):
            out+="  {} {} \n".format(self.types[i], row)
        out+="COLUMNS\n"
        # Aprime is of dimension (#constraints+1) * #var
        Aprime = np.concatenate(([self.c],self.A),axis=0)
        E = np.sum(Aprime, axis=0)
        row_names_prime = [self.objective_name]+self.row_names
        x_dim, y_dim = Aprime.shape[0],Aprime.shape[1]
        int_marker = 0
        new_int = True
        for j in range(y_dim):
            if self.col_types[j] == "integral" and new_int:
                out += f"     MARK{str(int_marker).zfill(4)}  'MARKER'                 'INTORG'\n"
                new_int = False
            for i in range(x_dim):
                if Aprime[i][j] == 0.0: continue # ignore variables that have all 0 coeff
                out +="     {}\t\t{}\t\t{}\n".format(self.col_names[j],row_names_prime[i],Aprime[i][j])
            if self.col_types[j] == "integral" and (j == y_dim - 1 or self.col_types[j+1] != "integral"):
                out += f"     MARK{str(int_marker).zfill(4)}  'MARKER'                 'INTEND'\n"
                int_marker += 1
                new_int = True
        out+="RHS\n"
        for rhs_name, rhs_bounds in self.rhs.items():
            for i in range(len(rhs_bounds)): 
                out += "    {}\t\t{}\t\t{}\n".format(rhs_name, self.row_names[i], rhs_bounds[i])
        out+="BOUNDS\n"

        # we only care about the first bound name
        bnd_lo = self.bnd[self.bnd_names[0]]['LO']
        bnd_up = self.bnd[self.bnd_names[0]]['UP']
        for i, col, lo, up in zip(range(len(self.col_names)), self.col_names, bnd_lo, bnd_up):
            if E[i] == 0.0: continue # ignore variables that have all 0 coeff
            if lo != 0:
                if lo != -np.inf:
                    lo = int(lo) if self.col_types[i] == "integral" else lo
                    sense = "LI" if self.col_types[i] == "integral" else "LO"
                    out += f" {sense} {self.bnd_names[0]}\t\t{col}\t\t{lo}\n"
                else:
                    out += f" MI {self.bnd_names[0]}\t\t{col}\n"
                    continue
            if up != np.inf:
                up = int(up) if self.col_types[i] == "integral" else up
                sense = "UI" if self.col_types[i] == "integral" else "UP"
                out += f" {sense} {self.bnd_names[0]}\t\t{col}\t\t{up}\n"
        out+="ENDATA"
        return out

    def __str__(self):
        return self.to_string()


def generate_prog(): 
    """
    Naive random generation of MPS programs. At the moment only generating LPs.   
    """ 
    name = "prog" 
    objsense = random.choice(["MAX","MIN"])
    objective_name = "OBJ"
    num_rows = random.randint(1,100) 
    row_names = []

    for i in range(num_rows):
        row_names.append("R"+str(i)) 

    num_cols = random.randint(1,100) 
    col_names =[] 
    for i in range(num_cols):
        col_names.append("C"+str(i)) 

    c = np.random.randint(-1000,1000, size=num_cols) 
    col_types = ["continous"]*num_cols
    types = ["L"]*num_rows
    # Modify the logic to generate A!!!
    # How many of them are 0?? - test to decide
    A = np.random.randint(-1000, 1000, size=(num_rows,num_cols))
    rhs_names=["RHS1"]
    rhs = {"RHS": np.random.randint(-1000, 1000, size=(num_rows,))}
    bnd_names =[]
    bnd = []
    return MPSProgram(name, 
              objsense, 
              objective_name, 
              row_names, 
              col_names, 
              col_types, 
              types, 
              c, 
              A, 
              rhs_names, 
              rhs, 
              bnd_names, 
              bnd)       


# Dominik: in this code snippet, I threw the generated programs at CPLEX   
# and observed its output. For a fuzzing approach, we would need to parse CPLEX's         
# Gurobi's and Gurobi's ouputs (i.e. the optimum value) and cross-check them.  
# As mentioned in the meeting, the crux will be to generate interesting MPS programs.  
# 
if __name__ == "__main__":
    prog = generate_prog()
    mps_file = open("mutant.mps", "w") 
    mps_file.write(prog.__str__())
    mps_file.flush()
    out = sp.getoutput('cplex -c "read mutant.mps" "optimize"')
    print(out)
