from time import time
import xml.etree.ElementTree as ET
from cplex import Cplex
import gurobipy as gp
from mps_util import VerbosePrint, eprint
import mip

# Solver status
OPTIMAL = 0
INFEASIBLE = 1
INF_OR_UNBD = 2
UNBOUNDED = 3
UNKNOWN = 4
TIME_LIMIT_EXCEEDED = 5

status_text = {
    OPTIMAL : "optimal",
    INFEASIBLE : "infeasible",
    INF_OR_UNBD : "infeasible or unbounded",
    UNBOUNDED : "unbounded",
    UNKNOWN : "unknown",
    TIME_LIMIT_EXCEEDED : "time limit exceeded",
}

MAX_TIME_LIMIT = 20

class Solver(object):
    def __init__(self, verbose = False, timeout = False) -> None:
        # by default, the solvers are very talky but we can make it less verbose
        self.verbose = verbose
        self.timeout = timeout
        self.stats = dict()
        for k in [OPTIMAL, INFEASIBLE, INF_OR_UNBD, UNBOUNDED, UNKNOWN, TIME_LIMIT_EXCEEDED]:
            self.stats[k] = 0
    
    def print_solver_stats(self) -> None:
        eprint("\n", self.__class__.__name__, " Stats:")
        for k in self.stats:
            eprint(status_text[k], " = ", self.stats[k], " times")
    
    def set_verbose(self, verbose):
        self.verbose = verbose
    
    def set_timeout(self, timeout):
        self.set_timeout = timeout

class CplexSolver(Solver):
    # https://www.tu-chemnitz.de/mathematik/discrete/manuals/cplex/doc/refman/html/appendixB.html
    status_code = {
        # LP
        1  : OPTIMAL,
        2  : UNBOUNDED,
        3  : INFEASIBLE,
        4  : INF_OR_UNBD,
        11 : TIME_LIMIT_EXCEEDED,
        # MIP
        101 : OPTIMAL, 
        102 : OPTIMAL,
        107 : TIME_LIMIT_EXCEEDED,
        108 : TIME_LIMIT_EXCEEDED,
        115 : INFEASIBLE, # https://www-eio.upc.edu/lceio/manuals/cplex-11/html/usrcplex/solveLP19.html
        103 : INFEASIBLE,
        118 : UNBOUNDED,
        119 : INF_OR_UNBD
    }

    def __init__(self, verbose = False, timeout = False, emphasis = 0) -> None:
        super().__init__(verbose, timeout)
        self.emphasis = emphasis

    def __call__(self, mps_file: str) -> dict:
        model = Cplex()
        model.set_results_stream(None)
        model.set_warning_stream(None)
        model.set_error_stream(None)
        model.set_log_stream(None)
        
        ps = model.create_parameter_set()
        ps.add(model.parameters.threads, 4) # somehow 32 threads is very slow
        ps.add(model.parameters.emphasis.mip, self.emphasis)

        if self.timeout:
            ps.add(model.parameters.timelimit, MAX_TIME_LIMIT)

        model.set_parameter_set(ps)

        with VerbosePrint(self.verbose):
            model.read(mps_file)
            model.solve()     

        if self.get_sol_status(model.solution) == INF_OR_UNBD:
            model.parameters.preprocessing.reduce.set(0)
            with VerbosePrint(self.verbose):
                model.read(mps_file)
                model.solve()

        ret = dict()
        ret["status"] = self.get_sol_status(model.solution)
        self.stats[ret["status"]] += 1

        if ret["status"] == OPTIMAL:
            ret["obj_val"] = self.get_obj_val(model.solution)
            ret["var_val"] = self.get_var_val(model.solution, model.variables)

        return ret

    def get_sol_status(self, sol) -> int:
        try:
            return CplexSolver.status_code[sol.get_status()]
        except KeyError:
            return UNKNOWN
    
    def get_obj_val(self, sol) -> float:
        return sol.get_objective_value()

    def get_var_val(self, sol, var) -> dict:
        ret = dict()
        for name in var.get_names():
            ret[name] = sol.get_values(name)
        return ret

    def get_sol_status_xml(self, sol: ET.ElementTree) -> int:
        root = sol.getroot()
        header = root.find('header')
        status = header.get('solutionStatusString')
        if status == 'optimal':
            return OPTIMAL
        elif status == 'infeasible':
            return INFEASIBLE
        else:
            return UNKNOWN

    def get_obj_val_xml(self, sol: ET.ElementTree) -> float:
        root = sol.getroot()
        header = root.find('header')
        return float(header.get('objectiveValue'))

    def get_var_val_xml(self, sol: ET.ElementTree) -> dict:
        root = sol.getroot()
        variables = root.find('variables')
        ret = dict()
        for var in variables.iter(tag = "variable"):
            name = var.get("name")
            val = float(var.get("value"))
            ret[name] = val
        return ret

class GurobiSolver(Solver):
    """ Gurobi Solver Wrapper
    Python interface documentation: 
    https://www.gurobi.com/documentation/9.5/refman/py_model.html#pythonclass:Model
    """
    # https://www.gurobi.com/documentation/9.5/refman/optimization_status_codes.html#sec:StatusCodes
    status_code = {
        2 : OPTIMAL,
        3 : INFEASIBLE,
        4 : INF_OR_UNBD,
        5 : UNBOUNDED,
        9 : TIME_LIMIT_EXCEEDED, 
    }

    def __init__(self, verbose = False, timeout = False) -> None:
        super().__init__(verbose, timeout)

    def __call__(self, mps_file: str) -> dict:
        with VerbosePrint(self.verbose):
            model = gp.read(mps_file)
            model.setParam("OutputFlag", False)
            if self.timeout:
                model.setParam(gp.GRB.Param.TimeLimit, MAX_TIME_LIMIT)
            model.optimize()
        
        if self.get_sol_status(model) == INF_OR_UNBD:
            # https://www.gurobi.com/documentation/9.5/refman/dualreductions.html#parameter:DualReductions
            with VerbosePrint(self.verbose):
                model.setParam("DualReductions", 0)
                model.optimize()
        
        if self.get_sol_status(model) == UNBOUNDED:
            # set the obj val to 0 and re-optimize
            # in Gurobi, unbound does not imply feasible
            with VerbosePrint(self.verbose):
                model.setObjective(0, model.ModelSense)
                model.optimize()

        ret = dict()
        ret["status"] = self.get_sol_status(model)
        self.stats[ret["status"]] += 1

        if ret["status"] == OPTIMAL:
            ret["obj_val"] = self.get_obj_val(model)
            ret["var_val"] = self.get_var_val(model)

        return ret

    def get_sol_status(self, model) -> int:
        return GurobiSolver.status_code[model.Status]

    def get_obj_val(self, model) -> float:
        return model.ObjVal

    def get_var_val(self, model) -> dict:
        ret = dict()
        for v in model.getVars():
            ret[v.VarName] = v.X
        return ret

class CBCSolver(Solver):
    """ CBC Solver Wrapper
    Python interface documentation: 
    https://python-mip.readthedocs.io/en/latest/quickstart.html
    """

    def __init__(self, verbose = False) -> None:
        super().__init__(verbose)

    def __call__(self, mps_file: str) -> dict:
        with VerbosePrint(False):
            model = mip.Model(solver_name=mip.CBC)
            model.verbose = 0
            model.read(mps_file)
            status = model.optimize()

        ret = dict()
        if status == mip.OptimizationStatus.OPTIMAL or \
            status == mip.OptimizationStatus.FEASIBLE: #for now marking this as optimal as well
            ret['status'] = OPTIMAL
            ret["obj_val"] = model.objective_value
        elif status == mip.OptimizationStatus.NO_SOLUTION_FOUND or \
            status == mip.OptimizationStatus.INFEASIBLE or \
            status == mip.OptimizationStatus.INT_INFEASIBLE:
            ret['status'] = INFEASIBLE
        elif status == mip.OptimizationStatus.UNBOUNDED:
            ret['status'] = UNBOUNDED
        else:
            ret["status"] = UNKNOWN
        
        self.stats[ret["status"]] += 1

        return ret


if __name__ == "__main__":
    mps_file = "mps/testprob_int.mps"
    cpx = CplexSolver()
    grb = GurobiSolver()
    cbc = CBCSolver()
    eprint(cpx(mps_file))
    eprint(grb(mps_file))
    eprint(cbc(mps_file))
    cpx.print_solver_stats()
    grb.print_solver_stats()
    cbc.print_solver_stats()
    
