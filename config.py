import argparse

def parse_args_function():
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        "--mut_methods", 
        nargs="+", 
        default=["ScaleMut", "SparseMatMut", "DenseMatMut", "FlipSignMut"],
        help="Mutation strategies to be used. \
            Available: ScaleMut, SparseMatMut, DenseMatMut, FlipSignMut"
    )
    parser.add_argument(
        "--ckpt_freq",
        type=int,
        default=10000,
        help="Number of iterations per checkpoint"
    )
    parser.add_argument(
        "--max_time",
        type=int,
        default=1,
        help="Maximum fuzzing time in seconds."
    )
    parser.add_argument(
        "--seed_path",
        type=str,
        default="seed/",
        help="Place where you store the seeds for fuzzing."
    )
    parser.add_argument(
        "--schedule",
        choices=["fifo", "pq"],
        type=str,
        default="fifo",
        help="Scheduler policy, fifo or pq"
    )
    parser.add_argument(
        "--save_path",
        type=str,
        default="mutated/",
        help="Place where you want to store the temporary mutants generated."
    )
    parser.add_argument(
        "--ckpt_path",
        type=str,
        default="ckpt/",
        help="Place where you want to store the checkpoints."
    )
    parser.add_argument(
        "--diff_path",
        type=str,
        default="diff/",
        help="Place where you want to store the mutants that cause differences."
    )
    parser.add_argument(
        "--mut_choice",
        choices=["fixed", "round_robin", "random"],
        type=str,
        default="fixed",
        help="When having more than one mutating strategy, how to pick it. \
            Available: fixed, round_robin, random."
    )
    parser.add_argument(
        "--solver1",
        choices=["Cplex", "Gurobi", "CBC"],
        type=str,
        default="Cplex",
        help="Avaialble solvers are Cplex, Gurobi and CBC"
    )
    parser.add_argument(
        "--solver2",
        choices=["Cplex", "Gurobi", "CBC"],
        type=str,
        default="Gurobi",
        help="Avaialble solvers are Cplex, Gurobi and CBC"
    )

    return parser

parser = parse_args_function()
