if [ $1 = "clean" ]; then
    rm -rf ckpt/
    # rm -rf diff/
    rm -rf mutated/
    rm clone*.log
    rm lsf.*
    exit
fi

time=14400

set -x
for mut in FlipSignMut MoreIntMut; do
    if [ $mut = "FlipSignMut" ]; then
        seed="seed/"
    else
        seed="seed2/"
    fi
    for policy in fifo pq; do
        if [ $1 = "local" ]; then
            python opt_fuzz.py \
                --mut_methods $mut \
                --seed_path $seed \
                --schedule $policy \
                --max_time $time 1>/dev/null
        elif [ $1 = "euler" ]; then
            bsub -W 270 -n 4 -R rusage[mem=2048] "python opt_fuzz.py \
                --mut_methods $mut \
                --seed_path $seed \
                --schedule $policy \
                --max_time $time 1>/dev/null"
        else
            echo "Must specify local or euler"
            exit
        fi
    done
done
set +x
