# FuzzOpt - Fuzzing LP/MIP Optimization Solvers

FuzzOpt is a light-weight black-box fuzzer for testing LP/MIP optimizers. It can run both locally and on Euler, ETH scientific computing platform. It is an extensible framework where you can easily test other solvers of interest, experiment with new mutation methods etc.

## Getting Started

Fuzzing usually takes prolonged time and optimization solvers are demanding in resources.
Therefore, it is desirable to deploy the fuzzing procedure on Euler.
First, we need to install the two optimizers of interest, Cplex and Gurobi.

### Cplex

To install Cplex, go to the download page of Cplex and choose the Linux version and download it to your local laptop.
Then you can copy the `.bin` file to the Euler whichever way you like (e.g., `scp` or FileZilla).
On Euler, execute the `.bin` file by

```bash
chmod a+x <filename>
./<filename>
```

Follow the instructions shown on the screen and when it asks where to install the binary, instead of using its default option, make the installation into your local home directory.
For example, you can specify the installation path as `/cluster/home/<user name>/ibm/ILOG/CPLEX_Studio_Community221/`.
This is because you do not have permission to install in `\opt`.
Once the installation is done, install the Cplex python API by

```bash
python /cluster/home/<user name>/ibm/ILOG/CPLEX_Studio_Community221/python/setup.py install
```

It is recommended to install your own python 3.8+ from Miniconda or alike.

Try if it works by typing `python -c "import cplex"`. If it prints nothing, it works.

### Gurobi

Moving on the Gurobi, Euler has Gurobi installed as modules so we only need to load them and install the python API.

For loading,

```bash
env2lmod # switch to the new software stack if you haven't
module load gurobi/9.5.1
```

To install the python API, do

```bash
python -m pip install gurobipy
```

Check if it works -- `python -c "import gurobipy"`.

Now you have all dependencies ready.

Note: you will need to load the Gurobi module every time you log into Euler. To avoid this, add the following line to `.bashrc`, which will load the module automatically.

```bash
module load gurobi/9.5.1
```

### python-mip with a CBC solver

Additionally we use python-mip with a CBC solver as a quorum for differences.
Install it by

```bash
pip install mip
```

### Other dependencies

Most of our depending python packages are shipped with Miniconda.
Our repository contained a submodule that needs to be fetched.
After cloning the repo, `cd` into the repo and do

```bash
git submodule update --init
```

## Get Seed Inputs

<!-- Fuzzing needs a selected pool of seeds to start with.
We will use the MIP test cases in <https://miplib.zib.de/tag_benchmark.html>.
To obtain the seeds, navigate to the root directory of the project

```bash
wget https://miplib.zib.de/downloads/benchmark.zip
unzip benchmark.zip -d benchmark/
cd benchmark
find . -type f -name '*.mps.gz' | xargs -I '{}' gzip -d '{}'
rm ../benchmark.zip
```

Now in the `benchmark/` directory, you can see all 240 files in `.mps` format.
Since all inputs add up to over 1GB, it is ignored by git. -->

Fuzzing needs a selected pool of seeds to start with.
For simplicity, we prepared a pool of seeds by transforming the MIP test cases
in <https://miplib.zib.de/tag_benchmark.html>.
The seeds can be obtained from <https://polybox.ethz.ch/index.php/s/Prdzjx8LPMUyvTC>.
Download from the link, and uncompress it put it in a convenient place.

## Run the fuzzer

Finally, we are able to run the fuzzer. FuzzOpt provides a wide variety of options
to customize the fuzzing. In the project directory, execute `python opt_fuzz.py -h`
to see the available options.

```bash
usage: opt_fuzz.py [-h] [--mut_methods MUT_METHODS [MUT_METHODS ...]] [--ckpt_freq CKPT_FREQ] [--max_time MAX_TIME] [--seed_path SEED_PATH] [--schedule {fifo,pq}] [--save_path SAVE_PATH] [--ckpt_path CKPT_PATH] [--diff_path DIFF_PATH] [--mut_choice {fixed,round_robin,random}] [--solver1 {Cplex,Gurobi,CBC}] [--solver2 {Cplex,Gurobi,CBC}]

optional arguments:
  -h, --help            show this help message and exit
  --mut_methods MUT_METHODS [MUT_METHODS ...]
                        Mutation strategies to be used. Available: ScaleMut, SparseMatMut, DenseMatMut, FlipSignMut
  --ckpt_freq CKPT_FREQ
                        Number of iterations per checkpoint
  --max_time MAX_TIME   Maximum fuzzing time in seconds.
  --seed_path SEED_PATH
                        Place where you store the seeds for fuzzing.
  --schedule {fifo,pq}  Scheduler policy, fifo or pq
  --save_path SAVE_PATH
                        Place where you want to store the temporary mutants generated.
  --ckpt_path CKPT_PATH
                        Place where you want to store the checkpoints.
  --diff_path DIFF_PATH
                        Place where you want to store the mutants that cause differences.
  --mut_choice {fixed,round_robin,random}
                        When having more than one mutating strategy, how to pick it. Available: fixed, round_robin, random.
  --solver1 {Cplex,Gurobi,CBC}
                        Avaialble solvers are Cplex, Gurobi and CBC
  --solver2 {Cplex,Gurobi,CBC}
                        Avaialble solvers are Cplex, Gurobi and CBC
```

If you use the CBC solver, we suggest redirect the standard output to `/dev/null`
because CBC logging messages cannot be disabled.

```bash
python opt_fuzz.py <options> 1>/dev/null
```

We also prepare a handy script `run.sh` to fuzz on Euler. It is recommended that to have
four cores at least for reasonable performance.

For each fuzzing run, we will create folders under `mutated/`, `ckpt/` and `diff/`
respectively to keep the all mutants and mutants that cause differences.

After fuzzing, you can replay the difference triggering mutants by first
finding the directory where the such mutants are stored and then run

```bash
python replay_diff.py <path_to_dir> 1</dev/null
```

## Customization

Adding a new solver to be tested or trying out a new mutation idea is fairly simple
in FuzzOpt.

To add a new solver, take a look at the existing solver wrappers in `optimizer.py`.
Make sure to (1) inherit the `Solver` class; (2) Match your solver's solution status
to our preset status codes.

To add a new mutation method, take a look at the existing mutations in `mps_mut.py`.
Make sure to inherit the `MutantMethod` class.

## Results

With FuzzOpt, we are able to find two different bugs in Cplex. The test inputs are
shown in `diff_mutants/`.
