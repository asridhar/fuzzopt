import random
from mps_util import load_mps_dict, print_mps_dict
import numpy as np

class MutantMethod(object):
    def __init__(self) -> None:
        self.mut_cnt = 0 # number of mutations produced
        self.mut_diff = [] # mutants that cause differences
    
    def inc_cnt(self):
        self.mut_cnt += 1

class ScaleMut(MutantMethod):
    def __init__(self, lo, hi) -> None:
        super().__init__()
        self.lo = lo
        self.hi = hi

    def __call__(self, mps):
        scale = self.lo + random.random()*(self.hi - self.lo)
        mps["obj_coeff"] *= scale
        mps["con_mat"] *= scale
        
        for v in mps["rhs"].values():
            v *= scale
        
        for v in mps["bnd"].values():
            for vv in v.values():
                vv *= scale
        
        self.inc_cnt()
        return mps

class SparseMatMut(MutantMethod):
    def __init__(self, prob) -> None:
        super().__init__()
        self.prob = prob

    def __call__(self, mps):
        shape = mps["con_mat"].shape
        size = shape[0] * shape[1]
        # https://stackoverflow.com/questions/48536969/how-to-randomly-set-elements-in-numpy-array-to-0
        indices = np.random.choice(size, replace=False, size=int(size * self.prob))
        mps["con_mat"][np.unravel_index(indices, shape)] = 0
        
        self.inc_cnt()
        return mps

class DenseMatMut(MutantMethod):
    def __init__(self, prob) -> None:
        super().__init__()
        self.prob = prob

    def __call__(self, mps):
        shape = mps["con_mat"].shape
        size = shape[0] * shape[1]
        indices = np.random.choice(size, replace=False, size=int(size * self.prob))
        for i, j in zip(*np.unravel_index(indices, shape)):
            mps["con_mat"][i,j] = np.random.random()
        
        self.inc_cnt()
        return mps

class FlipSignMut(MutantMethod):
    def __init__(self, prob) -> None:
        super().__init__()
        self.prob = prob

    def __call__(self, mps):
        shape = mps["con_mat"].shape
        size = shape[0] * shape[1]
        indices = np.random.choice(size, replace=False, size=int(size * self.prob))
        mps["con_mat"][np.unravel_index(indices, shape)] *= -1
        
        self.inc_cnt()
        return mps

class NoOpMut(MutantMethod):
    def __init__(self) -> None:
        super().__init__()

    def __call__(self, mps):
        return mps

class MoreIntMut(MutantMethod):
    def __init__(self, prob) -> None:
        super().__init__()
        self.prob = prob
    
    def __call__(self, mps):
        # Produce more integral constraints
        bnd_name = mps["bnd_names"][0]
        for i in range(len(mps["col_types"])):
            if mps["col_types"][i] != "integral" and random.random() < self.prob:
                mps["col_types"][i] = "integral"
                tmp = mps["bnd"][bnd_name]['LO'][i]
                mps["bnd"][bnd_name]['LO'][i] = int(tmp) if tmp != -np.inf else -np.inf
                tmp = mps["bnd"][bnd_name]['UP'][i]
                mps["bnd"][bnd_name]['UP'][i] = int(tmp) if tmp != np.inf else np.inf
        self.inc_cnt()
        return mps

# This mutation has to be applied with other mutations
# otherwise it's deterministic
class FlipObjSense(MutantMethod):
    def __init__(self) -> None:
        super().__init__()
    
    def __call__(self, mps):
        if mps["objsense"] == "" or mps["objsense"] == "MINIMIZE":
            mps["objsense"] = "MAXIMIZE"
        else:
            mps["objsense"] = "MINIMIZE"
        self.inc_cnt()
        return mps

if __name__ == "__main__":
    test_mps = "mps/testprob.mps"
    
    sm = ScaleMut(lo = -1, hi = 1)
    smm = SparseMatMut(prob = 0.5)
    dmm = DenseMatMut(prob = 0.9)
    fsm = FlipSignMut(prob = 0.9)
    mim = MoreIntMut(prob = 0.3)
    fos = FlipObjSense()
    
    print_mps_dict(sm(load_mps_dict(test_mps)))
    print_mps_dict(smm(load_mps_dict(test_mps)))
    print_mps_dict(dmm(load_mps_dict(test_mps)))
    print_mps_dict(fsm(load_mps_dict(test_mps)))
    print_mps_dict(mim(load_mps_dict(test_mps)))
    print_mps_dict(fos(load_mps_dict(test_mps)))