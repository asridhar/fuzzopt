NAME          TESTPROB
ROWS
 N  COST
 G  LIM1
 G  LIM2
COLUMNS
    XONE      COST                -1   LIM1                 1
    XONE      LIM2                 1
    YTWO      COST                -4   LIM1                 1
    ZTHREE    COST                -9   LIM2                 1
RHS
    RHS1      LIM1                 5   LIM2                10
BOUNDS
 UP BND1      XONE                 4
 LO BND1      YTWO                -1
 UP BND1      YTWO                 1
 FR BND1      ZTHREE
ENDATA
